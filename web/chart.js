var app = angular.module('emneeval-app', ['chart.js']);
var feedbackAddress = 'http://localhost:8080/feedback/';

var subjectController = app.controller('ChartController', ['$scope', '$http',
    function ($scope, $http) {


      $scope.semester = getParameterByName('semester');
      $scope.courseCode = getParameterByName('coursecode');
      $scope.evaluationType = getParameterByName('evaluationtype');
      if ($scope.evaluationType == null || $scope.evaluationType === '') {
        $scope.evaluationType = "Sluttevaluering";
      }

      $scope.labels = ['1', '2', '3', '4', '5'];
      $scope.series = [$scope.evaluationType];

      feedbackAddress += "course/" + $scope.courseCode + "/semester/" + $scope.semester + "/stats";

      console.log("GET " + feedbackAddress);
      $http({
        method: 'GET',
        url: feedbackAddress
      }).then(function successCallback(response) {
        $scope.happyData = Array(response.data.happy);
        $scope.learnedData = Array(response.data.learned);
        $scope.understandData = Array(response.data.understand);
        $scope.positiveComments = response.data.positiveComments;
        $scope.betterComments = response.data.betterComments;
        $scope.otherComments = response.data.otherComments;
        $scope.numberReplied = response.data.count;
      }, function failCallback(response) {
        alert(response);
      });


      /* The following function is taken from
      * https://stackoverflow.com/questions/53717122/how-to-write-regexp-to-get-a-paramater-from-url
      */
      function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
      }

    }
  ]
);
