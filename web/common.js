var app = angular.module('emneeval-app', []);
var feedbackAddress = 'http://localhost:8080/feedback/';

var subjectController = app.controller('SubjectController', ['$scope', '$http',
    function ($scope, $http) {

      $scope.happyList = [
        {text: 'svært fornøyd', value: 5, checked: false},
        {text: 'fornøyd', value: 4, checked: false},
        {text: 'verken fornøyd eller misfornøyd', value: 3, checked: true},
        {text: 'misfornøyd', value: 2, checked: false},
        {text: 'svært misfornøyd', value: 1, checked: false}
      ];

      $scope.learnedList = [
        {text: 'svært mye', value: 5, checked: false},
        {text: 'en del', value: 4, checked: false},
        {text: 'noe', value: 3, checked: true},
        {text: 'lite', value: 2, checked: false},
        {text: 'ingenting', value: 1, checked: false}
      ];

      $scope.understandList = [
        {text: 'svært godt', value: 5, checked: false},
        {text: 'godt', value: 4, checked: false},
        {text: 'forstår det', value: 3, checked: true},
        {text: 'ikke', value: 2, checked: false},
        {text: 'overhodet ikke', value: 1, checked: false}
      ];


      $scope.chosenHappy = null;
      $scope.chosenLearned = null;
      $scope.chosenUnderstand = null;
      $scope.positiveComment = null;
      $scope.betterComment = null;
      $scope.otherComment = null;
      $scope.follow = 'Campus';

      $scope.semester = getParameterByName('semester');
      $scope.courseCode = getParameterByName('coursecode');
      $scope.evaluationType = getParameterByName('evaluationtype');
      if ($scope.evaluationType == null || $scope.evaluationType === '') {
        $scope.evaluationType = "Sluttevaluering";
      }

      /* The following function is taken from
      * https://stackoverflow.com/questions/53717122/how-to-write-regexp-to-get-a-paramater-from-url
      */
      function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
      }

      $scope.postFeedback = function () {

        if ($scope.chosenHappy == null) {
          $scope.chosenHappy = {};
          $scope.chosenHappy.value = null;
        }
        if ($scope.chosenLearned == null) {
          $scope.chosenLearned = {};
          $scope.chosenLearned.value = null;
        }
        if ($scope.chosenUnderstand == null) {
          $scope.chosenUnderstand = {};
          $scope.chosenUnderstand.value = null;
        }

        $http({
          method: 'POST',
          url: feedbackAddress,
          data: {
            "courseCode": $scope.courseCode,
            "semester": $scope.semester,
            "evaluationType": $scope.evaluationType,
            "positiveComment": $scope.positiveComment,
            "betterComment": $scope.betterComment,
            "otherComment": $scope.otherComment,
            "happyLevel": $scope.chosenHappy.value,
            "learnedLevel": $scope.chosenLearned.value,
            "understandLevel": $scope.chosenUnderstand.value,
            "follow": $scope.follow
          }
        }).then(function successCallback(response) {
          console.log("POST postFeedback [" + feedbackAddress +
            "] returned " + JSON.stringify(response));
        }, function errorCallback(response) {
          alert("Kunne ikke laste opp tilbakemelding. " + JSON.stringify(response));
          console.log("POST postFeedback error [" + feedbackAddress + "] returned " + response);
        });
      };

      $scope.updateChosenHappy = function (happy) {
        for (i = 0; i < $scope.happyList.length; i++) {
          $scope.happyList[i].checked = false;
        }
        console.log("Chosen happy" + JSON.stringify(happy));
        $scope.chosenHappy = happy;
      };

      $scope.updateChosenLearned = function (learned) {
        for (i = 0; i < $scope.learnedList.length; i++) {
          $scope.learnedList[i].checked = false;
        }
        console.log("Chosen learned" + JSON.stringify(learned));
        $scope.chosenLearned = learned;
      };

      $scope.updateChosenUnderstand = function (understand) {
        for (i = 0; i < $scope.understandList.length; i++) {
          $scope.understandList[i].checked = false;
        }
        console.log("Chosen understand " + JSON.stringify(understand));
        $scope.chosenUnderstand = understand;
      }
    }
  ]
);
