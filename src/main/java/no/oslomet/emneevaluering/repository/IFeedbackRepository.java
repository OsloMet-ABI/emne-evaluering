package no.oslomet.emneevaluering.repository;


import no.oslomet.emneevaluering.model.Feedback;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IFeedbackRepository
        extends PagingAndSortingRepository<Feedback, Long> {
    List<Feedback> findByCourseCodeAndSemester(String courseCode,
                                               String semester);

    Long countByCourseCodeAndSemester(
            String courseCode, String semester);

    Long countByHappyLevelAndCourseCodeAndSemester(
            Integer happyLevel, String courseCode, String semester);

    Long countByLearnedLevelAndCourseCodeAndSemester(
            Integer learnedLevel, String courseCode, String semester);

    Long countByUnderstandLevelAndCourseCodeAndSemester(
            Integer learnedLevel, String courseCode, String semester);

    List<Feedback> findByCourseCodeAndSemesterAndEvaluationType(
            String courseCode, String semester, String evalutionType);
}
