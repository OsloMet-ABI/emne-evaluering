package no.oslomet.emneevaluering.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Stats {

    private List<String> happy = new ArrayList<>();
    private List<String> learned = new ArrayList<>();
    private List<String> understand = new ArrayList<>();
    private String evaluationType;

    private List<String> otherComments = new ArrayList<>();
    private List<String> positiveComments = new ArrayList<>();
    private List<String> betterComments = new ArrayList<>();

    private Long count;

    public List<String> addHappy(String value) {
        happy.add(value);
        return happy;
    }

    public List<String> addUnderstand(String value) {
        understand.add(value);
        return understand;
    }

    public List<String> addLearned(String value) {
        learned.add(value);
        return learned;
    }

    public String setEvaluationType(String evaluationType) {
        this.evaluationType = evaluationType;
        return this.evaluationType;
    }

    public List<String> addOtherComment(String otherComment) {
        otherComments.add(otherComment);
        return otherComments;
    }

    public List<String> addPositiveComment(String positiveComment) {
        positiveComments.add(positiveComment);
        return positiveComments;
    }

    public List<String> addBetterComment(String betterComment) {
        betterComments.add(betterComment);
        return betterComments;
    }

    public List<String> getHappy() {
        return happy;
    }

    public void setHappy(List<String> happy) {
        this.happy = happy;
    }

    public List<String> getLearned() {
        return learned;
    }

    public void setLearned(List<String> learned) {
        this.learned = learned;
    }

    public List<String> getUnderstand() {
        return understand;
    }

    public void setUnderstand(List<String> understand) {
        this.understand = understand;
    }

    public String getEvaluationType() {
        return evaluationType;
    }

    public List<String> getOtherComments() {
        return otherComments;
    }

    public void setOtherComments(List<String> otherComments) {
        this.otherComments = otherComments;
    }

    public List<String> getPositiveComments() {
        return positiveComments;
    }

    public void setPositiveComments(List<String> positiveComments) {
        this.positiveComments = positiveComments;
    }

    public List<String> getBetterComments() {
        return betterComments;
    }

    public void setBetterComments(List<String> betterComments) {
        this.betterComments = betterComments;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Stats{" +
                "happy=" + happy +
                ", learned=" + learned +
                ", understand=" + understand +
                ", evaluationType='" + evaluationType + '\'' +
                ", otherComments=" + otherComments +
                ", positiveComments=" + positiveComments +
                ", betterComments=" + betterComments +
                ", count=" + count +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Stats)) return false;
        Stats stats = (Stats) o;
        return Objects.equals(happy, stats.happy) &&
                Objects.equals(learned, stats.learned) &&
                Objects.equals(understand, stats.understand) &&
                Objects.equals(evaluationType, stats.evaluationType) &&
                Objects.equals(otherComments, stats.otherComments) &&
                Objects.equals(positiveComments, stats.positiveComments) &&
                Objects.equals(betterComments, stats.betterComments) &&
                Objects.equals(count, stats.count);
    }

    @Override
    public int hashCode() {
        return Objects.hash(happy, learned, understand, evaluationType,
                otherComments, positiveComments, betterComments, count);
    }
}
