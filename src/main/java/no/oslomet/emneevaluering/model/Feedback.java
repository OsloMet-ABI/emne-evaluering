package no.oslomet.emneevaluering.model;


import org.hibernate.annotations.GenericGenerator;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.Objects;

import static javax.persistence.GenerationType.AUTO;

@Entity
public class Feedback
        extends ResourceSupport {

    @Id
    @GeneratedValue(
            strategy = AUTO,
            generator="native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    @Column(nullable = false, updatable = false)
    private Long primaryId;

    @Column(length = 5000)
    private String positiveComment;

    @Column(length = 5000)
    private String betterComment;

    @Column(length = 5000)
    private String otherComment;

    @Column
    private Integer happyLevel;

    @Column
    private Integer learnedLevel;

    @Column
    private Integer understandLevel;

    @Column
    private String semester;

    @Column
    private String courseCode;

    // Is it a week / midway / end-of-semester evaluation
    @Column
    private String evaluationType;

    // Is it a week / midway / end-of-semester evaluation
    @Column
    private Date createdDate;

    public Long getPrimaryId() {
        return primaryId;
    }

    public void setPrimaryId(Long primaryId) {
        this.primaryId = primaryId;
    }

    public String getPositiveComment() {
        return positiveComment;
    }

    public void setPositiveComment(String positiveComment) {
        this.positiveComment = positiveComment;
    }

    public String getBetterComment() {
        return betterComment;
    }

    public void setBetterComment(String betterComment) {
        this.betterComment = betterComment;
    }

    public String getOtherComment() {
        return otherComment;
    }

    public void setOtherComment(String otherComment) {
        this.otherComment = otherComment;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public Integer getHappyLevel() {
        return happyLevel;
    }

    public void setHappyLevel(Integer happyLevel) {
        this.happyLevel = happyLevel;
    }

    public Integer getLearnedLevel() {
        return learnedLevel;
    }

    public void setLearnedLevel(Integer learnedLevel) {
        this.learnedLevel = learnedLevel;
    }

    public Integer getUnderstandLevel() {
        return understandLevel;
    }

    public void setUnderstandLevel(Integer understandLevel) {
        this.understandLevel = understandLevel;
    }

    public String getEvaluationType() {
        return evaluationType;
    }

    public void setEvaluationType(String evaluationType) {
        this.evaluationType = evaluationType;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "primaryId=" + primaryId +
                ", positiveComment='" + positiveComment + '\'' +
                ", betterComment='" + betterComment + '\'' +
                ", otherComment='" + otherComment + '\'' +
                ", happyLevel=" + happyLevel +
                ", learnedLevel=" + learnedLevel +
                ", understandLevel=" + understandLevel +
                ", semester='" + semester + '\'' +
                ", courseCode='" + courseCode + '\'' +
                ", evaluationType='" + evaluationType + '\'' +
                ", createdDate='" + createdDate + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Feedback)) return false;
        if (!super.equals(o)) return false;
        Feedback feedback = (Feedback) o;
        return Objects.equals(primaryId, feedback.primaryId) &&
                Objects.equals(positiveComment, feedback.positiveComment) &&
                Objects.equals(betterComment, feedback.betterComment) &&
                Objects.equals(otherComment, feedback.otherComment) &&
                Objects.equals(happyLevel, feedback.happyLevel) &&
                Objects.equals(learnedLevel, feedback.learnedLevel) &&
                Objects.equals(understandLevel, feedback.understandLevel) &&
                Objects.equals(semester, feedback.semester) &&
                Objects.equals(courseCode, feedback.courseCode) &&
                Objects.equals(evaluationType, feedback.evaluationType) &&
                Objects.equals(createdDate, feedback.createdDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), primaryId, positiveComment,
                betterComment, otherComment, happyLevel, learnedLevel,
                understandLevel, semester, courseCode, evaluationType,
                createdDate);
    }
}
