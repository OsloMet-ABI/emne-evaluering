package no.oslomet.emneevaluering.controller;

import no.oslomet.emneevaluering.model.Feedback;
import no.oslomet.emneevaluering.model.Stats;
import no.oslomet.emneevaluering.repository.IFeedbackRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("feedback")
public class FeedbackController {

    private static final Logger logger =
            LoggerFactory.getLogger(FeedbackController.class);

    private IFeedbackRepository feedbackRepository;

    public FeedbackController(IFeedbackRepository feedbackRepository) {
        this.feedbackRepository = feedbackRepository;
    }

    @GetMapping(value = {"/course/{courseCode}/semester/{semester}",
            "/course/{courseCode}/semester/{semester}/type/{type}"})
    public ResponseEntity<List<Feedback>>
    getFeedbackForCourseAndSemester(@PathVariable("courseCode")
                                            String courseCode,
                                    @PathVariable("semester")
                                            String semester,
                                    @PathVariable(name = "type", required = false)
                                            String type) {
        if (type == null) {
            return new ResponseEntity<>
                    (feedbackRepository.findByCourseCodeAndSemester(
                            courseCode, semester), OK);
        } else {
            return new ResponseEntity<>
                    (feedbackRepository.
                            findByCourseCodeAndSemesterAndEvaluationType(
                                    courseCode, semester, type), OK);
        }
    }

    @GetMapping(value = {"/course/{courseCode}/semester/{semester}/stats"})
    public ResponseEntity<Stats>
    getFeedbackStatsForCourseAndSemester(@PathVariable("courseCode")
                                                 String courseCode,
                                         @PathVariable("semester")
                                                 String semester) {
        Stats stats = new Stats();
        stats.setCount(feedbackRepository.
                countByCourseCodeAndSemester(courseCode, semester));
        for (int i = 1; i < 6; i++) {
            stats.addHappy(feedbackRepository.
                    countByHappyLevelAndCourseCodeAndSemester(
                            i, courseCode, semester).toString());
            stats.addLearned(feedbackRepository.
                    countByLearnedLevelAndCourseCodeAndSemester(
                            i, courseCode, semester).toString());
            stats.addUnderstand(feedbackRepository.
                    countByUnderstandLevelAndCourseCodeAndSemester(
                            i, courseCode, semester).toString());
        }
        for (Feedback feedback :
                feedbackRepository.findByCourseCodeAndSemester(
                        courseCode, semester)) {
            stats.addPositiveComment(feedback.getPositiveComment());
            stats.addBetterComment(feedback.getBetterComment());
            stats.addOtherComment(feedback.getOtherComment());
        }
        return new ResponseEntity<>(stats, OK);
    }

    @PostMapping
    public ResponseEntity<Feedback>
    postFeedbackForCourseAndSemester(@RequestBody Feedback feedback) {
        feedback.setCreatedDate(new Date());
        return new ResponseEntity<>
                (feedbackRepository.save(feedback), CREATED);
    }
}
